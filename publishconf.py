#!/usr/bin/env python
# -*- coding: utf-8 -*- #

import sys
sys.path.append(os.curdir)
from pelicanconf import *

SITEURL = 'https://bits.debian.org'

DELETE_OUTPUT_DIRECTORY = True

# Following items are often useful when publishing

# Uncomment following line for absolute URLs in production:
#RELATIVE_URLS = False

#DISQUS_SITENAME = ""
#GOOGLE_ANALYTICS = ""
