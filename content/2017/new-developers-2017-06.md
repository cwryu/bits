Title: New Debian Developers and Maintainers (May and June 2017)
Slug: new-developers-2017-06
Date: 2017-07-02 14:30
Author: Jean-Pierre Giraud
Tags: project
Status: published

The following contributors got their Debian Developer accounts in the last two months:

  * Alex Muntada (alexm)
  * Ilias Tsitsimpis (iliastsi)
  * Daniel Lenharo de Souza (lenharo)
  * Shih-Yuan Lee (fourdollars)
  * Roger Shimizu (rosh)

The following contributors were added as Debian Maintainers in the last two months:

  * James Valleroy
  * Ryan Tandy
  * Martin Kepplinger
  * Jean Baptiste Favre
  * Ana Cristina Custura
  * Unit 193

Congratulations!
