Title: Debian turns 23!
Date: 2016-08-16 14:30
Tags: debian, birthday
Slug: debian-turns-23
Author: Laura Arjona Reina
Status: published

Today is Debian's 23rd anniversary. If you are close to any of the cities
[celebrating Debian Day 2016](https://wiki.debian.org/DebianDay/2016), you're
very welcome to join the party!

If not, there's still time for you to organize a little celebration or
contribution to Debian. For example, you can have a look at the [Debian timeline](https://timeline.debian.net/)
and learn about the history of the project. If you notice that some piece of
information is still missing, feel free to add it to the timeline.

Or you can scratch your creative itch and suggest a wallpaper
to be part of the [artwork for the next release](https://wiki.debian.org/DebianDesktop/Artwork/Stretch).

Our favorite operating system is the result of all the work we have done together.
Thanks to everybody who has contributed in these 23 years, and happy birthday Debian!
