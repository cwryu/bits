Title: Thème Debian : appel à propositions pour Debian 10 (Buster)
Date: 2018-06-17 13:30
Tags: artwork, buster, cfp, debian
Slug: buster-artwork-cfp
Author: Jonathan Carter
Lang: fr
Translator: Jean-Pierre Giraud
Status: published

Voici l'appel à propositions officiel pour le thème du cycle de Buster.

Pour disposer des détails les plus récents, veuillez vous référer à la page du
[wiki](https://wiki.debian.org/DebianDesktop/Artwork/Buster).

Nous voudrions profiter de cette occasion pour remercier Juliette
Taka Belin pour avoir créé le
[thème Softwaves pour Stretch](https://wiki.debian.org/DebianArt/Themes/softWaves).

La date limite pour les propositions est le 5 septembre 2018

Le thème est habituellement choisi en se basant sur ce qui paraît le plus :

* « Debian » : il est vrai que ce n'est pas le concept le mieux défini, dans la
  mesure où chacun a son sentiment sur ce que représente Debian pour eux ;
* « possible à intégrer sans corriger le logiciel de base » :
  autant nous aimons les thèmes follement excitants, certains nécessiteraient
  un gros travail d'adaptation des thèmes GTK+ et la correction de GDM/GNOME ;
* « clair et bien conçu » : sans être quelque chose qui devient ennuyeux à
  regarder au bout d'un an. Parmi les bons thèmes, on peut citer Joy, Lines et
  Softwaves.

Si vous souhaitez disposer plus d'informations, veuillez utiliser la
[liste de diffusion Debian Desktop](https://lists.debian.org/debian-desktop/).
