Title: Une croisière pour DebConf20
Date: 2018-04-01 20:15
Tags: debian, announce, debconf
Slug: debconf20-in-a-cruise
Author: Debian Publicity Team
Translator: French Translation Team
Lang: fr
Status: published

Les dernières éditions de [DebConf](https://debconf.org), la conférence annuelle de Debian,
se sont tenues à des endroits aussi différents que Heidelberg (Allemagne), Le Cap (Afrique du Sud) et Montréal
(Canada). L'été prochain la [DebConf18](https://debconf18.debconf.org) aura lieu à Hsinchu (Taïwan)
et le lieu de la [DebConf19](https://wiki.debconf.org/wiki/DebConf19) a déjà été choisi : Curitiba (Brésil).
Durant toutes ces années, une idée était dans l'air (plus précisément dans les canaux IRC de Debian), celle
d'organiser une DebConf sur un paquebot.
Aujourd'hui, le projet Debian est heureux d'annoncer qu'un groupe de contributeurs de Debian
se sont rassemblés pour proposer une offre concrète pour une *DebConf20 en croisière*.

L'équipe Croisière est convaincue de sa capacité à produire une offre détaillée et solide pour la fin de
l'année. Cependant, un plan succinct et des préparatifs ont déjà été réalisés :
la conférence aura lieu en juillet et en août 2020, durant une croisière autour du monde,
sous la forme d'une **« rolling conference »**. Cela signifie que les contributeurs de Debian pourront choisir
quand arriver et partir en embarquant et débarquant dans un des ports où le bateau fera escale.
Un DebCamp centré sur un sprint pour le développement des [mélanges Debian](https://www.debian.org/blends/)
et une « journée porte ouverte » avec des « install party » sous la mer et d'autres activités intéressantes
pour le grand public sont aussi prévues.

Il y aura une rencontre pour discuter des détails de la candidature durant la DebConf18 à Hsinchu.
L'équipe a également engagé des discussions avec diverses compagnies de croisières et des fournisseurs de
réseaux satellitaires afin d'explorer toutes les options de lieux et de connexions possibles pour la conférence.
Les parties intéressées peuvent contacter press@debian.org pour rejoindre l'équipe Croisière dans la préparation de
la future conférence.
