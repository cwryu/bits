Title: Debian celebrates 26 years, Happy DebianDay!
Date: 2019-08-16 20:12
Tags: debian birthday
Slug: debian-turns-26
Author: Donald Norwood
Status: published


26 years ago today in a single post to the comp.os.linux.development newsgroup,
Ian Murdock announced the completion of a brand new Linux release named [Debian](https://groups.google.com/forum/#!msg/comp.os.linux.development/Md3Modzg5TU/xty88y5OLaMJ).

Since that day we’ve been into outer space, typed over 1,288,688,830 lines of 
code, spawned over 300 derivatives, were enhanced with 6,155
known [contributors](https://contributors.debian.org/contributors/flat), and filed over 975,619 bug reports.

We are home to a community of thousands of users around the globe, we gather to 
host our annual Debian Developers Conference [DebConf](www.debconf.org)
which spans the world in a different country each year, and of course today's 
many [DebianDay](https://wiki.debian.org/DebianDay/2019) celebrations held around the world.

It's not too late to throw an impromptu DebianDay celebration or to go and join
one of the many celebrations already underway.

As we celebrate our own anniversary, we also want to celebrate our many 
contributors, developers, teams, groups, maintainers, and users. It is all of 
your effort,  support, and drive that continue to make Debian 
truly: The universal operating system.


Happy DebianDay!
