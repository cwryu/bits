Title: La derivada de Debian «Lernstick» guanya el premi «Swiss Open Source Education».
Slug: lernstick-award
Date: 2015-11-10 10:00
Author: Laura Arjona Reina
Translator: Adrià García-Alzórriz, Lluís Gili
Tags: Lernstick, debian, derivatives, awards
Lang: ca
Status: draft

La derivada de Debian [Lernstick](FIXME_SEE_NOTE) ha guanyat el premi "Swiss Open Source Education".

Lernstick és una derivada de Debian "Live" orientada a les escoles. L'usen principalment a Suïssa, Àustria i Alemanya.
Lernstick ofereix un aprenentatge mòbil i segur, i un entorn de treball que normalment s'usa en medis extraïbles (llapis USB, discos durs USB, targetes SD, etc.) i opcionalment la podríeu instal·lar en un disc dur intern.

La distribució Lernstick conté programes educatius, programari multimèdia, un paquet ofimàtic complet, jocs, programes pel tractament digital d'imatges, aplicacions per a Internet i té un espai per a la informació personal.

L'ha desenvolupat la Universitat de Ciències Aplicades del Nord-oest de Suïssa i educa.ch. Ha estat en constant evolució des dels seus inicis. El 9 d'octubre del 2015 van publicar-ne la darrera versió, basada en Debian 8 Jessie.

Els premis [CH Open Source Awards](http://www.ossawards.ch/) s'otorguen a empreses, organismes públics, comunitats de programari lliure i particulars que han estat decisius i innovadors desenvolupant o iniciant programari de codi obert. Lernstick ha guanyat el premi especial Open Source Education Award de l'edició de 2015, patrocinat per IBM.


Enllaços per a informació addicional:
* [Pàgina de Lernstick del dens de derivades de  Debian ](https://wiki.debian.org/Derivatives/Census/Lernstick)
* [Lernstick - resum en anglès](http://www.imedias.ch/projekte/lernstick/lernstick_abstract_english.cfm)

NOTA
Dos enllaços de Lernstick
* Del cens de Derivades: http://www.imedias.ch/projekte/lernstick/
* Dels CH OSS Awards : https://lernstick.educa.ch/#
(Ambdós en alemany)
